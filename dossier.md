Dossier **Help To Help**
===================
---


####**Functionele Specificaties**

>- One Page Webapplication
-- Bookmark routes
-- of een webapplicatie bestaande uit verschillende pagina’s
- Preloaders/Loaders
-- Om de app in te laden
-- Tijdens het inladen van JSON(P)
-- Lokale data bestanden
- De meeste inhoud wordt beheerd in data bastanden en dynamisch ingeladen
- Adaptive images, video’s en sounds
- Google Maps integratie of gelijkaardig
-- Custom look-and-feel
- GEO-location
-- Toon de locatie van de gebruiker op een Map
-- Hou rekening met deze locatie in andere onderdelen van deze app
- Social Media Bookmarking (Open Graph)
- Animaties via SVG en/of Canvas
- Lokaal caching van data en bronbestanden (cache manifest)
- Gebruiker ervaart een interactief webapplicatie
- Gebruiker kan favoriete data lokaal bewaren
- Gebruiker kan de webapplicatie bookmarken in browser, bureaublad en als native app in het overzicht
- Automation verplicht!
-- Componenten worden via Bower toegevoegd in de components folder van de app folder
-- SASS bestanden worden automatisch omgezet in corresponderende CSS-bestanden
-- CSS-bestanden worden met elkaar verbonden in één bestand en geminified
-- De JS code wordt automatisch nagekeken op syntax fouten JS-bestanden worden met elkaar verbonden in één bestand en geminified
-- De dist-folder wordt automatisch aangevuld met bestanden en folders via Grunt of Gulp
-- Screenshots van de verschillende breekpunten worden automatisch uitgevoerd via Phantom, Casper of andere bibliotheken



####**Technische specificaties**

**Frontend**
>- Core technologies: HTML5, CSS3 en JavaScript
- Template engines: Jade, Haml, Swig of Handlebars
- Storage: JSON bestanden, localstorage en/of IndexedDB
- Bibliotheken: jQuery, underscore.js, lodash.js, crossroads.js, js-signals, Hasher.js, ...
- Andere bibliotheken worden hierin aangevuld tijdens het semester!
- Uitzonderingen mogelijk betreffende JavaScript bibliotheken mogelijk mits toelating!

**Backend**
>- Er is geen backend aanwezig in deze webapplicatie.

---


###**Persona's**

> ![enter image description here](images/PersonaDeVuystMichael.png "De Vuyst Michael")
>![enter image description here](images/PersonaSarahBossuwe.png "Sarah Bossuwe")
>![enter image description here](images/PersonaSteveCooman.png "Steve Cooman")

---

###**ideëenborden**
> ###** Bram Billiet **
> ![enter image description here](images/Ideeenbord-Bram.gif "Ideeenbord Bram")

>---
> ###** Kevin De Vuyst **
>![enter image description here](images/ideeenbord-Kevin.jpg "ideeenbord Kevin")

>---
> ###** Fabian Nelis **
> ![enter image description here](images/ideeenbord-Fabian.gif "ideeenbord Fabian")

---
#### **Sitemap**
![enter image description here](images/Sitemap.gif "Sitemap")

---

### **Wireframes**
> ###** Bram Billiet **
>![enter image description here](images/Wireframe-Bram.gif "Wireframe-Bram") 

>---
> ###** Kevin De Vuyst **
>![enter image description here](images/Wireframe-Kevin.gif "Wireframe-Kevin")

>---
> ###** Fabian Nelis **
>![enter image description here](images/Wireframe-Fabian.gif "Wireframe-Fabian")

---



###**Style Tyles**
> ###** Bram Billiet **
>
>![enter image description here](images/Style-tile-Bram-Billiet.jpg "Style-tile-Bram-Billiet")

>---
> ###** Kevin De Vuyst **
> ![enter image description here](images/Style_Tile_Kevin.jpg "Style_Tile_Kevin")

>---
> ###** Fabian Nelis **
> ![enter image description here](images/Style-tile-Fabian-Nelis.jpg "Style_Tile_Fabian")

>---
> ###**Final Style Tile**
> ![enter image description here](images/Style_Tile_Final.gif "Style_Tile")

---


###**Visual Designs**

>![enter image description here](images/Concept-iPhone6---2-Info.jpg "iPhone6---2-Info")
> ![enter image description here](images/Concept-iPhone6---3-Quiz.jpg "iPhone6---3-Quiz")
> ![enter image description here](images/Concept-iPhone6---4-Choose-by-globe.jpg "iPhone6---4-Choose-by-globe")
>![enter image description here](images/Concept-iPhone6---6-Choose-manually.jpg "iPhone6---6-Choose-manually")
> ![enter image description here](images/Concept-iPhone6---4.2-DetailPagina-land.jpg "iPhone6---4.2-DetailPagina-land")
> ![enter image description here](images/Concept-iPhone6---5.-DetailPagina-Organisation.jpg "iPhone6---5.-DetailPagina-Organisation")

---

###**Screenshots eindresultaat**
>---
> ###**Home Desktop**
> 
>![enter image description here](images/Screenshot-home.png "Home desktop pagina")
>---
> ###**Home Mobile**
> 
>![enter image description here](images/Screenshot-home-2.png "Home mobile pagina")
>---
> ###**Quiz Desktop**
> 
>![enter image description here](images/Screenshot-enquete.png "quiz desktop pagina")
>---
> ###**Quiz Mobile**
> 
>![enter image description here](images/Screenshot-enquete-2.png "quiz mobile pagina")
>---
> ###**Area Desktop**
> 
>![enter image description here](images/Screenshot-area.png "area pagina")
>---
> ###**Area Mobile**
> 
>![enter image description here](images/Screenshot-enquete-2.png "area mobile pagina")
>---
> ###**Organisation Desktop**
> 
>![enter image description here](images/Screenshot-organisation.png "organisation pagina")
>---
> ###**Organisation Mobile**
> 
>![enter image description here](images/Screenshot-organisation-2.png "organisation mobile pagina")
>---
> ###**Menu Moblie**
> 
>![enter image description here](images/Screenshot-Menu.png "Menu mobile")



###**Screenshots Snippets**


> ####**HTML**
> ![enter image description here](images/Snippet-html-1.png "html 1")
> 
> ![enter image description here](images/Snippet-html-2.png "html 2")
> 
> ![enter image description here](images/Snippet-html-3.png "html 3")
> 
>---
> ####**CSS**
> ![enter image description here](images/Snippet-css-1.png "css 1")
> 
> ![enter image description here](images/Snippet-css-2.png "css 2")
> 
> ![enter image description here](images/Snippet-css-3.png "css 3")
> 
>---
> ####**JS**
> ![enter image description here](images/Snippet-js-1.png "js 1")

---


